# **Multigame** #

Multigame consists of 6 board games that can be played by 2 people: Reversi, Mill,  Renju, Halma, RSP(rock scissors paper) and Sea Battle. About these game’s rules you can read in application.

To start the game, first you have to register. Be sure to enter a valid email, because you will have a letter to confirm registration. You can create an unlimited count of different games or join to waiting games.

To demonstrate the administrator mode, enter the game under the login: Vasya and password: qwerty.

Enjoy the game

The project was developed on Java using next technologies, languages, frameworks and libraries:
Back-end: Java Core, PostgreSQL, mongoDB, Sping Boot, Spring framework, Spring MVC, Spring Data + Hibernate, Spring Security, Spring-test, JUnit, Hamcrest, Mockito, Log4J, hibernate-validator, Jasypt, Jackson, Web sockets.
Front-end: JavaScript, JSP, CSS, jquery, bootstrap
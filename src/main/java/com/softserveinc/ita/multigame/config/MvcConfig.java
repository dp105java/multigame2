package com.softserveinc.ita.multigame.config;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.Locale;

/**
 * Class represents basic MVC configuration for web app.
 * Contains config for {@link ViewResolver} and setting for i18n (localization)
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
@ComponentScan(basePackages = {
        "com.softserveinc.ita.multigame.controllers",
        "com.softserveinc.ita.multigame.model",
        "com.softserveinc.ita.multigame.repositories",
        "com.softserveinc.ita.multigame.services"
})
public class MvcConfig extends WebMvcConfigurerAdapter {

        @Override
        public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
                configurer.enable();
        }

        @Bean
        public ViewResolver jspViewResolver() {
                InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
                viewResolver.setViewClass(JstlView.class);
                viewResolver.setPrefix("/WEB-INF/views/");
                viewResolver.setSuffix(".jsp");
                return viewResolver;
        }

        @Bean(name = "messageSource")
        public ReloadableResourceBundleMessageSource messageSourceBean() {
                ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
                messageSource.setBasenames("/WEB-INF/i18n/messages", "/WEB-INF/i18n/header-messages");
                messageSource.setFallbackToSystemLocale(false);
                messageSource.setDefaultEncoding("UTF-8");
                return messageSource;
        }

        @Bean(name = "localeResolver")
        public CookieLocaleResolver localeResolverBean() {
                CookieLocaleResolver resolver = new CookieLocaleResolver();
                resolver.setDefaultLocale(new Locale("en"));
                return resolver;
        }

        @Override
        public void addInterceptors(InterceptorRegistry registry) {
                LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
                localeInterceptor.setParamName("lang");
                registry.addInterceptor(localeInterceptor);
        }

}

package com.softserveinc.ita.multigame.controllers.rest;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameListManagerImpl;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * RestController for common actions of all games
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 *
 * GET    http://localhost:8080/api/v.0.1/board?gameId=1
 * GET    http://localhost:8080/api/v.0.1/tips?gameId=1
 * POST   http://localhost:8080/api/v.0.1/maketurn?gameId=1&turn=ROCK
 */

@RestController
@RequestMapping("/api/v.0.1")
public class GameAjaxRestController {
    private Logger logger = Logger.getLogger(GameAjaxRestController.class);

    @Autowired
    private GameListService gameListService;

      @GetMapping(value = "/board", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getBoard(@RequestParam("gameId") Long gameId) {
        logger.info("In getBoard gameId is: " + gameId);
        Game game = gameListService.getGame(gameId);
        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Object object = game.getGameEngine().getBoard();
        return new ResponseEntity<>(object, HttpStatus.OK);
    }

    @GetMapping(value = "/tips", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TipDTO> getTips(@RequestParam("gameId") Long gameId, @SessionAttribute Player player) {
        logger.info("In getTips gameId is: " + gameId);
        TipDTO dto = gameListService.getTips(gameId, player);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/maketurn", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> makeTurn(@RequestParam("gameId") Long gameId,
                         @RequestParam("turn") String turn,
                         @SessionAttribute Player player) {
        logger.info("In makeTurn gameId is: " + gameId + " turn is: " + turn);
        gameListService.makeTurn(gameId, player, turn);
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

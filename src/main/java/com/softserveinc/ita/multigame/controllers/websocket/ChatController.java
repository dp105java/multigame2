package com.softserveinc.ita.multigame.controllers.websocket;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Controller for Common Chat
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */

@Controller
public class ChatController {

    private Set<String> players = new CopyOnWriteArraySet<>();

    @MessageMapping("/send")
    @SendTo("/chat/getMessage")
    public ChatMessage chatting(ChatMessage message) throws Exception {
        return message;
    }

    @MessageMapping("/add")
    @SendTo("/chat/addAndGetPlayers")
    public Set<String> addAndGetPlayers(String player) throws Exception {
        players.add(player);
        return players;
    }

    @MessageMapping("/remove")
    @SendTo("/chat/removeAndGetPlayers")
    public Set<String> removeAndGetPlayers(String player) throws Exception {
        players.remove(player);
        return players;
    }

}

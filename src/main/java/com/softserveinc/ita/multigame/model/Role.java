package com.softserveinc.ita.multigame.model;

/**
 * Roles of  {@link Player}
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
public enum Role {
    ROLE_ADMIN, ROLE_PLAYER
}

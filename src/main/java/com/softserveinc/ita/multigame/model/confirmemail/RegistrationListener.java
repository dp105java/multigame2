package com.softserveinc.ita.multigame.model.confirmemail;

import com.softserveinc.ita.multigame.model.Player;

import com.softserveinc.ita.multigame.services.VerificationTokenService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Send email to {@link Player} with unique verification token
 * Implements {@link ApplicationListener}. Parameterized by {@link OnRegistrationCompleteEvent}
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private Logger logger = Logger.getLogger(RegistrationListener.class);

    @Autowired
    private VerificationTokenService verificationTokenService;
    @Autowired
    private JavaMailSender mailSender;
    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        Player player = event.getPlayer();
        String token = UUID.randomUUID().toString();

        verificationTokenService.createVerificationToken(player, token);

        String recipientAddress = player.getEmail();
        String subject = "Registration Confirmation";
        String url = event.getAppPath() + "/registrationConfirm?token=" + token;
        String message = "use this link to confirm your email \n";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + url);
        mailSender.send(email);
        logger.debug("email was sent to player #" + player.getId());
    }
}

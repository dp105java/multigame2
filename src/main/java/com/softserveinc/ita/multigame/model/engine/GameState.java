package com.softserveinc.ita.multigame.model.engine;

/**
 * Represents states of the game that change during the game after right action
 *
 * @author Alexandr Reuta
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
public enum GameState {
    WAIT_FOR_FIRST_PLAYER,
    WAIT_FOR_SECOND_PLAYER,
    WAIT_FOR_FIRST_PLAYER_TURN,
    WAIT_FOR_SECOND_PLAYER_TURN,
    WAIT_FOR_FIRST_PLAYER_DROP,
    WAIT_FOR_SECOND_PLAYER_DROP,
    FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER,
    FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER,
    FINISHED_WITH_DRAW,
    UNDEFINED
}

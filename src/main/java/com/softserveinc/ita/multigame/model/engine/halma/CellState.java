package com.softserveinc.ita.multigame.model.engine.halma;

/**
 * Represents states of {@link Cell}
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
public enum CellState {
    BUSY_BY_FIRST_PLAYER,
    BUSY_BY_SECOND_PLAYER,
    EMPTY
}


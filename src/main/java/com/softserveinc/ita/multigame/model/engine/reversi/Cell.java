package com.softserveinc.ita.multigame.model.engine.reversi;

/**
 * Class Cell is a wrapper, which contains coordinates of current cell
 * on the {@link com.softserveinc.ita.multigame.model.engine.reversi.Board}
 * and the value.
 * if value is 0 - cell is empty
 * if value is 1 - cell contains white pawn
 * if value is 2 - cell contains black pawn
 *
 * @author Igor Khlapon igor.boxmails@gmail.com
 * @since v1.0
 */

public class Cell {

    private int x;
    private int y;
    private int value;

    public Cell(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{\"x\": " + x + ", \"y\": " + y + ", \"value\": " + value + "}";
    }
}

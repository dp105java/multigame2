package com.softserveinc.ita.multigame.model.engine.reversi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Util methods for {@link com.softserveinc.ita.multigame.model.engine.reversi.Reversi} game
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 * @since v1.0
 */

public class ReversiUtil {

    /**
     * turnParser() parse the literal turn, made by user, into array coordinates
     * @param playerTurn represents literal turn (i.e. e4; g3; a4 etc.)
     */
    public static Map<Integer, Integer> turnParser(String playerTurn) {
        Map<Integer, Integer> turn = new HashMap<>();
        char charX = playerTurn.charAt(0); //letter
        char charY = playerTurn.charAt(playerTurn.length() - 1); //digit from 1 to 8
        int x = -1, y = -1;

        //x parsing
        //65-72; 97-104 letters from a to h and from A to H
        if (charX >= 65 && charX <= 72) {
            x = getArrayIndexByCharCode(charX, 65, 72);
        } else if (charX >= 97 && charX <= 104) {
            x = getArrayIndexByCharCode(charX, 97, 104);
        }

        //y parsing
        //49-56 - digits from 1 to 8
        int temp = 7;
        for (int i = 49; i <= 56; i++) {
            if (i == charY) {
                y = temp;
                break;
            } else {
                temp--;
            }
        }

        turn.put(x, y);

        return turn;
    }

    private static int getArrayIndexByCharCode(char character, int beginRange, int endRange) {
        int index = -1;

        int temp = 0;
        for (int i = beginRange; i <= endRange; i++) {
            if (i == character) {
                index = temp;
                break;
            } else {
                temp++;
            }
        }

        return index;
    }


    /**
     * This util method is needed for creation List of cells that are already have a pawn on it
     *
     * List of cells (pureCells) returns when getBoard() method
     * of {@link com.softserveinc.ita.multigame.model.engine.reversi.Reversi} invokes.
     * It is needed for creation JSON, which will be passed on UI for board rendering
     */
    public static List<Cell> arrayToCellsListParser(int[][] field) {
        ArrayList<Cell> cells = new ArrayList<>();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                cells.add(new Cell(j, i, field[i][j]));
            }
        }
        ArrayList<Cell> pureCells = new ArrayList<>();
        for (Cell cell : cells) {
            if (cell.getValue() != 0) {
                pureCells.add(cell);
            }
        }
        return pureCells;
    }

}

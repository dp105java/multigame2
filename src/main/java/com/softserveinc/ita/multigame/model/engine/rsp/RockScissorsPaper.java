package com.softserveinc.ita.multigame.model.engine.rsp;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

import static com.softserveinc.ita.multigame.model.engine.GameState.*;



/**
 * Engine for Rock-Paper-Scissors-Lizard-Spock game
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */
public class RockScissorsPaper extends GenericGameEngine {

    static final String ROCK = "ROCK";
    static final String SCISSORS = "SCISSORS";
    static final String PAPER = "PAPER";
    static final String LIZARD = "LIZARD";
    static final String SPOCK = "SPOCK";

    static String firstPlayerChoice;
    static String secondPlayerChoice;

    public RockScissorsPaper() {
        super();
    }

    public RockScissorsPaper(Long id) {
        super(id);
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        /*Method is not implemented, because our turn logic is
        * implemented in changeGameState method*/
        return true;
    }

    @Override
    protected GameState changeGameState(Player player, String turn) {

        if (gameState == WAIT_FOR_FIRST_PLAYER_TURN) {
            firstPlayerChoice = turn;
            return WAIT_FOR_SECOND_PLAYER_TURN;
        }

        if (gameState == WAIT_FOR_SECOND_PLAYER_TURN) {
            secondPlayerChoice = turn;
        }

        if (firstPlayerWins()) {
            return FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }

        if (secondPlayerWins()) {
            return FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }

        if (happensDraw()) {
            return FINISHED_WITH_DRAW;
        } else return WAIT_FOR_FIRST_PLAYER_TURN;
    }

    static boolean firstPlayerWins() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(LIZARD)
                || firstPlayerChoice.equals(SPOCK) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(LIZARD) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(SPOCK) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(LIZARD)
                || firstPlayerChoice.equals(LIZARD) && secondPlayerChoice.equals(SPOCK)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(SPOCK);
    }

    static boolean secondPlayerWins() {
        return secondPlayerChoice.equals(ROCK) && firstPlayerChoice.equals(SCISSORS)
                || secondPlayerChoice.equals(SCISSORS) && firstPlayerChoice.equals(PAPER)
                || secondPlayerChoice.equals(PAPER) && firstPlayerChoice.equals(ROCK)
                || secondPlayerChoice.equals(ROCK) && firstPlayerChoice.equals(LIZARD)
                || secondPlayerChoice.equals(SPOCK) && firstPlayerChoice.equals(ROCK)
                || secondPlayerChoice.equals(LIZARD) && firstPlayerChoice.equals(PAPER)
                || secondPlayerChoice.equals(SPOCK) && firstPlayerChoice.equals(SCISSORS)
                || secondPlayerChoice.equals(SCISSORS) && firstPlayerChoice.equals(LIZARD)
                || secondPlayerChoice.equals(LIZARD) && firstPlayerChoice.equals(SPOCK)
                || secondPlayerChoice.equals(PAPER) && firstPlayerChoice.equals(SPOCK);
    }

    static boolean happensDraw() {
        return secondPlayerChoice.equals(ROCK) && firstPlayerChoice.equals(ROCK)
                || secondPlayerChoice.equals(SCISSORS) && firstPlayerChoice.equals(SCISSORS)
                || secondPlayerChoice.equals(PAPER) && firstPlayerChoice.equals(PAPER)
                || secondPlayerChoice.equals(LIZARD) && firstPlayerChoice.equals(LIZARD)
                || secondPlayerChoice.equals(SPOCK) && firstPlayerChoice.equals(SPOCK);
    }
}

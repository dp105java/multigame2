<%-- @author Vladimir Serdiuk vvserdiuk@gmail.com --%>
<%@ page import="com.softserveinc.ita.multigame.model.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <title>Players</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="shortcut icon" href="${contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${contextPath}/resources/js/admin.js"></script>

</head>
<body>
<%@include file="header.jsp" %>

<div class="container jumbotron">
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <button class="emailButton" data-toggle="modal" data-target="#emailModal" >
            Mass mailing <img src="${contextPath}/resources/img/email-button.png" style="height: 5%">
        </button>
    </sec:authorize>
    <ul class="list-group">
        <c:forEach items="${players}" var="player">
            <li id="${player.id}"  class="list-group-item">
                <a href="profile?id=${player.id}" style="font-size: 30px">
                    <img class="avatar-small" src="${player.avatar}" alt="avatar">
                    <c:choose>
                        <c:when test="${not empty player.fullName}">
                            ${player.fullName}
                        </c:when>
                        <c:otherwise>
                            ${player.login}
                        </c:otherwise>
                    </c:choose>
                </a>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <button class="emailButton" data-toggle="modal" data-target="#emailModal" >
                        <img src="${contextPath}/resources/img/email-button.png" style="height: 5%">
                    </button>

                    <c:choose>
                        <c:when test="${player.role eq 'ROLE_ADMIN'}">
                            <button class="adminButton" >
                                <img src="${contextPath}/resources/img/admin.png" style="height: 5%">
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button class="playerButton">
                                <img src="${contextPath}/resources/img/player.png" style="height: 5%">
                            </button>
                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${player.enabled}">
                            <button class="banButton" >
                                <img src="${contextPath}/resources/img/ban-button.png" style="height: 5%">
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button class="unbanButton">
                                <img src="${contextPath}/resources/img/unban-button.png" style="height: 5%">
                            </button>
                        </c:otherwise>
                    </c:choose>

                    <button class="deleteButton">
                        <img src="${contextPath}/resources/img/delete-button.png" style="height: 5%">
                    </button>
                </sec:authorize>
            </li>
        </c:forEach>
    </ul>
</div>


<div class="modal" id="emailModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="emailForm" class="form-group" action="${contextPath}/admin/sendEmail" method="POST" >
                    <input id="playerId" name="id" type="hidden" value=""/>
                    <label for="subject">Subject: </label>
                    <input id="subject" class="form-control" name="subject" type="text"/>
                    <label for="text">Text: </label>
                    <input id="text" class="form-control input-lg" name="text" type="text"/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="sendEmailButton" type="button" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:message code="label.seabattle.letsset" var="let1"/>
<spring:message code="label.seabattle.letsset2" var="let2"/>
<spring:message code="label.seabattle.cancel" var="cancel"/>
<spring:message code="label.seabattle.confirm" var="confirm"/>
<spring:message code="label.seabattle.def" var="def"/>
<!DOCTYPE html>
<html>
<head>
    <title>Set Ships</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link href="${pageContext.request.contextPath}/resources/css/seaBattle/set.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/seaBattle/sweetalert.css" rel="stylesheet">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/seaBattle/target.ico" width="16"
          height="16" type="image/x-icon">

    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/setShips.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/sweetalert.min.js"></script>
</head>
<body>
<div class="container-fluid bs-cont">
    <!--Top Block-->
    <div class="row bs-row">
        <h3>${let1}, ${player.login}, ${let2} # ${gameId}!</h3>
    </div><!--End Of Top Block-->

    <!--Middle-->
    <div class="row bs-row-c">
        <div class="col-md-2 bs-col"></div>

        <div class="col-md-6 bs-col">
            <div class="jumbotron jumbo-c" id="board">
                <!--Field Here-->
            </div>
        </div>

        <div class="col-md-3 bs-col">
            <div class="jumbotron jumbo-c-r">
                <input type="hidden" id="idOfGame" value="${gameId}"/>

                <div id="sideBar">

                </div>

                <br/>
                <a href="cancel?gameId=${gameId}">
                    <button type="submit" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove">&nbsp;</span>${cancel}</button>
                </a>
                <br/><br/><br/><br/><br/><br/><br/><br/>
                <button type="submit" class="btn btn-info" onclick="confirm()"><span class="glyphicon glyphicon-saved">&nbsp;</span>${confirm}
                </button>
                <br/>
                <a href="default?gameId=${gameId}">
                    <button type="submit" class="btn btn-default"><span
                            class="glyphicon glyphicon-wrench">&nbsp;</span>${def}</button>
                </a>

            </div>
        </div>
        <div class="col-md-1 bs-col">
        </div>
    </div><!--End of Middle-->

    <!--Bot Block-->
    <div class="row bs-row" id="botTip">
    </div><!--End Of \bot Block-->

</div>
</body>
</html>
$(document).ready(function () {
    makeActive();
    refreshLists();
    setInterval(function () {
        refreshLists();
        makeActive();
    }, 3000);
});

function makeActive() {
    $('#gameList').addClass("active");
    console.log('make gameList active');
}


function refreshLists() {
    $.ajax({
        method: 'GET',
        url: 'ajax/lists',
        dataType: 'json',
        success: function (data) {
            $('#playingGames').empty();
            $('#createdGames').empty();
            $('#waitingGames').empty();

            $('#playingGames').append('<b>Playing games</b>');
            $('#createdGames').append('<b>Created games</b>');
            $('#waitingGames').append('<b>Waiting games</b>');

            $.each(data.playingGames, function (k, v) {
                $('#playingGames').append(
                    '<a href="' + v.gameType +
                    '?gameId=' + v.gameId +
                    '" class="list-group-item">' +
                    '<img class="logo-img" src=' + v.logo +
                    '> #' + v.gameId +
                    ' owner: ' + v.owner.login +
                    '</a>');
            });

            $.each(data.createdGames, function (k, v) {
                $('#createdGames').append(
                    '<a href="#" class="list-group-item">' +
                    '<img class="logo-img" src=' + v.logo +
                    '> #' + v.gameId +
                    ' owner: ' + v.owner.login +
                    '</a>');
            });

            $.each(data.waitingGames, function (k, v) {
                $('#waitingGames').append(
                    '<a href="wantjoin?gameId=' + v.gameId +
                    '&gameOwnerId=' + v.owner.id +
                    '" class="list-group-item">' +
                    '<img class="logo-img" src=' + v.logo +
                    '> #' + v.gameId +
                    ' owner: ' + v.owner.login +
                    '</a>');
            });
        },
        error: function () {
            //alert('Error!');
            console.log('error during refreshList() method running');
        }
    });
}
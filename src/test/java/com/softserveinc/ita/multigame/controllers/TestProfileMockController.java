package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing Profile controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */
public class TestProfileMockController {

    private Long playerId = 5L;
    Player firstPlayer = new Player(5L, "Alex", "111");
    Game game1 = new Game(firstPlayer, GameType.RSP);
    Long gameId = 8L;
    GameHistory gameHistory = new GameHistory(game1);
    List<GameHistory> historyList = Arrays.asList(gameHistory);
    List<Player> players = Arrays.asList(firstPlayer);

    private PlayerService playerService;
    private GameHistoryService gameHistoryService;
    private ProfileController controller;

    @Before
    public void init() {
        playerService = Mockito.mock(PlayerService.class);
        gameHistoryService = Mockito.mock(GameHistoryService.class);
        controller = new ProfileController(playerService, gameHistoryService);
    }

    @Test
    public void testingProfilePageRedirectionToJsp() {
        //setup
        Mockito.when(playerService.get(playerId)).thenReturn(firstPlayer);
        Mockito.when(gameHistoryService.getGameHistoriesForPlayer(firstPlayer)).thenReturn(historyList);
        ModelMap model = new ModelMap();
        model.addAttribute("firstPlayer", firstPlayer);
        model.addAttribute("historyList", historyList );
        firstPlayer = playerService.get(playerId);
        //Execute
        String viewName =
                controller.profilePage( playerId, model);
        //Verify
        assertEquals(viewName, "playerProfile");
        assertEquals(model.get("firstPlayer"), firstPlayer);
        assertEquals(model.get("historyList"),historyList);
    }

    @Test
    public void testingGetAllProfilesAndRedirectToJsp() {
        //setup
        Mockito.when(playerService.getAll()).thenReturn(players);
        ModelMap model = new ModelMap();
        model.addAttribute("players", players);
        //Execute
        String viewName =
                controller.getAllProfiles(model);
        //Verify
        assertEquals(viewName, "playersList");
        assertEquals(model.get("players"), players);
    }



}

package com.softserveinc.ita.multigame.controllers.rest;

import com.google.gson.Gson;
import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.List;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link ProfileRestController}
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

@Category(IntegrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testmg")
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ProfileRestControllerTest {

    private final String BASE_URL = "/api/v.0.1";

    @Autowired
    private ProfileRestController controllerToTest;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private Gson gson;

    private MockMvc mockMvc;
    private Player p1;
    private Player p2;

    @Before
    public void setUp() {
        Player player1 = new Player("AAA", "111");
        player1.setEmail("aaa@gmail.com");
        playerService.saveOrUpdate(player1);
        Player player2 = new Player("BBB", "222");
        player2.setEmail("bbb@gmail.com");
        playerService.saveOrUpdate(player2);

        p1 = playerService.getByLogin("AAA");
        p2 = playerService.getByLogin("BBB");

        mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
        controllerToTest = mock(ProfileRestController.class);
    }

    @Test
    public void testCreatePlayer() throws Exception {
        Player player = p1;

        mockMvc.perform(put(BASE_URL + "/player")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(player))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isCreated());
    }

    @Test
    public void testReadPlayersList() throws Exception {
        List<Player> playerList = playerService.getAll();

        mockMvc.perform(get(BASE_URL + "/players")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void testReadPlayer() throws Exception {

        mockMvc.perform(get(BASE_URL + "/player/" +p2.getId())
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void testUpdatePlayer() throws Exception {
        Player player = p1;
        player.setId(22L);
        player.setEmail("dsd@ru");
        player.setLogin("CCCC");

        mockMvc.perform(post(BASE_URL + "/player/" +player.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(player))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk());

    }

    @Test
    public void testDeletePlayer() throws Exception {

        mockMvc.perform(delete(BASE_URL + "/player/" +p2.getId())
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk());
    }
}

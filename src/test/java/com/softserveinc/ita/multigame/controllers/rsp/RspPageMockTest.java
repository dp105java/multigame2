package com.softserveinc.ita.multigame.controllers.rsp;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing RSP game controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

public class RspPageMockTest {

    Player firstPlayer = new Player(1L, "Alex", "111");
    Player secondPlayer = new Player(2L, "Ben", "111");
    Long gameId = 2L;
    Game game1 = new Game(firstPlayer, GameType.RSP);
    TipDTO dto = new TipDTO(secondPlayer,"message");

    private GameListService gameListService;
    private RspPageController rspPageController;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        rspPageController = new RspPageController(gameListService);
    }

    @Test
    public void testingThatGetPageRedirectsToTheViewAndPutsAttributesTrue() {
        //setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game1);
        GameEngine gameEngine = game1.getGameEngine();
        firstPlayer = gameEngine.getFirstPlayer();
        secondPlayer = gameEngine.getSecondPlayer();
        gameEngine.setFirstPlayer(firstPlayer);
        gameEngine.setSecondPlayer(secondPlayer);
        ModelMap model = new ModelMap();

        //Execute
        String viewName =
                rspPageController.getPage(firstPlayer, gameId, model);
        //Verify
        assertEquals(viewName, "rsp/rspGame");
        assertEquals(model.get("firstPlayer"), firstPlayer);
        assertEquals(model.get("gameId"),gameId);
    }

    @Test
    public void testRefreshPlayerTurnReturnsProperDTO() {
        //setup
        Mockito.when(gameListService.getTips(gameId, firstPlayer)).thenReturn(dto);
        //Execute
        TipDTO dto1 = rspPageController.refreshPlayerTurn(firstPlayer,gameId);
        //Verify
        assertEquals(dto1, dto);
    }
}

package com.softserveinc.ita.multigame.model.engine.reversi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link com.softserveinc.ita.multigame.model.engine.reversi.Color}
 *
 * @author Igor Khlaponin
 */
public class TestColor {

    @Test
    public void checkGetWhiteColor(){
        int expectedColor = Color.WHITE;
        int acturalColor = Color.getColor(1);
        assertEquals(expectedColor, acturalColor);
    }

    @Test
    public void checkGetBlackColor(){
        int expectedColor = Color.BLACK;
        int actualColor = Color.getColor(2);
        assertEquals(expectedColor, actualColor);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void checkGetColorByWrongNumber(){
        Color.getColor(3);
    }

    @Test
    public void checkGetColorByRightNumber(){
        assertEquals(Color.EMPTY_CELL, Color.getColor(0));
        assertEquals(Color.WHITE, Color.getColor(1));
        assertEquals(Color.BLACK, Color.getColor(2));
    }

    @Test
    public void checkGetOpponentBlackColorIfCurrentColorIsWhite(){
        int expectedColor = Color.BLACK;
        int actualColor = Color.getOpponentColor(Color.WHITE);
        assertEquals(expectedColor, actualColor);
    }

    @Test
    public void checkGetOpponentWhiteColorIfCurrentColorIsBlack(){
        int expectedColor = Color.WHITE;
        int actualColor = Color.getOpponentColor(Color.BLACK);
        assertEquals(expectedColor, actualColor);
    }

}
